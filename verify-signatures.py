#!/usr/bin/env python

import pexpect
import sys
import os
import itertools

which = sys.argv[1]
mailmap = sys.argv[2]

# vague approximation of correct. solution: review changes to mailmap

key_lines = [
        l.split(b' # ')[-1] 
        for l in open(mailmap, 'rb') 
        if b' # ' in l]
valid_keys = []
for line in key_lines:
    valid_keys.extend(map(bytes.strip, line.split(b' ')))

commits = pexpect.run('git rev-list --full-history ' + which)

# Eventually this should be split out into a library, with a way of verifying
# only commits up to some particular landmark (probably 'most recent signed
# tag')

metapath = os.path.join(os.path.dirname(mailmap), '.revoked-keys')
revoked = {}
for line in open(metapath, 'rb'):
    if line.startswith(b'#') or not l:
        continue
    parts = line.split(b' ')
    revoked[parts[0]] = parts[1:]

toplevel = pexpect.run('git rev-parse --show-toplevel').strip()

try:
    ignored_commits =  [l.strip() 
            for l in open(os.path.join(toplevel, '.signing-exceptions'), 'rb') 
            if l and not l.startswith('#')]
except Exception as e:
    ignored_commits = [] # fail safe, usual error is no .signing-exceptions

try:
    ignored_commits =  [l.strip() for l in open(os.path.join(pexpect.run('git rev-parse --show-toplevel').strip(),
        '.signing-exceptions'), 'rb') if l and not l.startswith(b'#')]
except Exception as e:
    ignored_commits = [] # fail safe, usual error is no .signing-exceptions

for commit in commits.split():
    if commit not in ignored_commits:
        gpgdump = pexpect.run('git verify-commit --raw ' + commit)
        sigs = [l for l in gpgdump.split(b'\r\n') if b'VALIDSIG' in l]
        if not sigs:
            print("ERROR: no valid signature for commit {}".format(commit))
            sys.exit(1)
        elif len(sigs) > 1:
            print("More than one valid signature?")
        keyid = sigs[0].split(b' ')[-1].strip()
        if keyid not in valid_keys:
            print("ERROR: commit {} signed with unrecognized key {}".format(commit, keyid))
            sys.exit(2)
        elif keyid in revoked:
            if not any(map(lambda s: pexpect.run('git merge-base '
                    '--is-ancestor {safe} {commit}'.format(safe=s,
                    commit=commit), withexitstatus=True)[1] == 0, revoked[keyid])):
                print("ERROR: commit {} signed with revoked key {} and not reachable from any safe commit".format(commit, keyid))
                sys.exit(1)
        sys.stdout.write('.')
        sys.stdout.flush()

print(" All good")
