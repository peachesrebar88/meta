[[setting-up-a-build-environment]]
Setting up a build environment
------------------------------

Setting up a build environment for Robigalia is easy! First, clone the "devbox" repository:

    git clone --recursive git@gitlab.com:robigalia/devbox.git robigalia

Set the `RUST_TARGET_PATH` environment variable in your shell configuration
to `path/to/robigalia/sel4-targets`. Make sure you have a recent Rust nightly installed. 
If you're using `rustup`, run something like:

    rustup default nightly-$DATE

The `$DATE` we use for CI can be found https://gitlab.com/robigalia/runner/blob/master/Dockerfile#L33[approximately here].

After that, just one last step:

    cargo install xargo

Now, you should be able to `cd path/to/robigalia/hello-world` and execute:

    xargo build --target x86_64-sel4-robigalia

To build the hello-world project.

If you wish, you can also use the https://gitlab.com/robigalia/runner/tree/master[Docker container] that we use for CI.

[[troubleshooting]]
Troubleshooting
---------------

If you tried following these steps but came across an error, *please* come
to our https://robigalia.org/contact.html[IRC channel or mailing list] so we
can help you, and add more details to this section. Thanks!
